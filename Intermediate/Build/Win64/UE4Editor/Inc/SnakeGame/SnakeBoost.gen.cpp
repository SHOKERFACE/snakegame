// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/SnakeBoost.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeBoost() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeBoost_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeBoost();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_APlayerPawnBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ASnakeBoost::execInteract)
	{
		P_GET_OBJECT(AActor,Z_Param_Interactor);
		P_GET_UBOOL(Z_Param_bIsHead);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Interact(Z_Param_Interactor,Z_Param_bIsHead);
		P_NATIVE_END;
	}
	void ASnakeBoost::StaticRegisterNativesASnakeBoost()
	{
		UClass* Class = ASnakeBoost::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Interact", &ASnakeBoost::execInteract },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ASnakeBoost_Interact_Statics
	{
		struct SnakeBoost_eventInteract_Parms
		{
			AActor* Interactor;
			bool bIsHead;
		};
		static void NewProp_bIsHead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsHead;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Interactor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ASnakeBoost_Interact_Statics::NewProp_bIsHead_SetBit(void* Obj)
	{
		((SnakeBoost_eventInteract_Parms*)Obj)->bIsHead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ASnakeBoost_Interact_Statics::NewProp_bIsHead = { "bIsHead", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SnakeBoost_eventInteract_Parms), &Z_Construct_UFunction_ASnakeBoost_Interact_Statics::NewProp_bIsHead_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ASnakeBoost_Interact_Statics::NewProp_Interactor = { "Interactor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SnakeBoost_eventInteract_Parms, Interactor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ASnakeBoost_Interact_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASnakeBoost_Interact_Statics::NewProp_bIsHead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASnakeBoost_Interact_Statics::NewProp_Interactor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASnakeBoost_Interact_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SnakeBoost.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASnakeBoost_Interact_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASnakeBoost, nullptr, "Interact", nullptr, nullptr, sizeof(SnakeBoost_eventInteract_Parms), Z_Construct_UFunction_ASnakeBoost_Interact_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ASnakeBoost_Interact_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASnakeBoost_Interact_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASnakeBoost_Interact_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASnakeBoost_Interact()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASnakeBoost_Interact_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASnakeBoost_NoRegister()
	{
		return ASnakeBoost::StaticClass();
	}
	struct Z_Construct_UClass_ASnakeBoost_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerPawnBaseOwner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerPawnBaseOwner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnakeBoost_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ASnakeBoost_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ASnakeBoost_Interact, "Interact" }, // 1631656744
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeBoost_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SnakeBoost.h" },
		{ "ModuleRelativePath", "SnakeBoost.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeBoost_Statics::NewProp_PlayerPawnBaseOwner_MetaData[] = {
		{ "ModuleRelativePath", "SnakeBoost.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnakeBoost_Statics::NewProp_PlayerPawnBaseOwner = { "PlayerPawnBaseOwner", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeBoost, PlayerPawnBaseOwner), Z_Construct_UClass_APlayerPawnBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnakeBoost_Statics::NewProp_PlayerPawnBaseOwner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeBoost_Statics::NewProp_PlayerPawnBaseOwner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASnakeBoost_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeBoost_Statics::NewProp_PlayerPawnBaseOwner,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASnakeBoost_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASnakeBoost, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnakeBoost_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnakeBoost>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnakeBoost_Statics::ClassParams = {
		&ASnakeBoost::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ASnakeBoost_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeBoost_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASnakeBoost_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeBoost_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnakeBoost()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnakeBoost_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnakeBoost, 2123772963);
	template<> SNAKEGAME_API UClass* StaticClass<ASnakeBoost>()
	{
		return ASnakeBoost::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnakeBoost(Z_Construct_UClass_ASnakeBoost, &ASnakeBoost::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ASnakeBoost"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnakeBoost);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
