// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef SNAKEGAME_SnakeBoost_generated_h
#error "SnakeBoost.generated.h already included, missing '#pragma once' in SnakeBoost.h"
#endif
#define SNAKEGAME_SnakeBoost_generated_h

#define Snake_Source_SnakeGame_SnakeBoost_h_16_SPARSE_DATA
#define Snake_Source_SnakeGame_SnakeBoost_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInteract);


#define Snake_Source_SnakeGame_SnakeBoost_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInteract);


#define Snake_Source_SnakeGame_SnakeBoost_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBoost(); \
	friend struct Z_Construct_UClass_ASnakeBoost_Statics; \
public: \
	DECLARE_CLASS(ASnakeBoost, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBoost) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeBoost*>(this); }


#define Snake_Source_SnakeGame_SnakeBoost_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBoost(); \
	friend struct Z_Construct_UClass_ASnakeBoost_Statics; \
public: \
	DECLARE_CLASS(ASnakeBoost, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBoost) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeBoost*>(this); }


#define Snake_Source_SnakeGame_SnakeBoost_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBoost(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBoost) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBoost); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBoost); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBoost(ASnakeBoost&&); \
	NO_API ASnakeBoost(const ASnakeBoost&); \
public:


#define Snake_Source_SnakeGame_SnakeBoost_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBoost(ASnakeBoost&&); \
	NO_API ASnakeBoost(const ASnakeBoost&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBoost); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBoost); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBoost)


#define Snake_Source_SnakeGame_SnakeBoost_h_16_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_SnakeGame_SnakeBoost_h_13_PROLOG
#define Snake_Source_SnakeGame_SnakeBoost_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_SnakeGame_SnakeBoost_h_16_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_SnakeGame_SnakeBoost_h_16_SPARSE_DATA \
	Snake_Source_SnakeGame_SnakeBoost_h_16_RPC_WRAPPERS \
	Snake_Source_SnakeGame_SnakeBoost_h_16_INCLASS \
	Snake_Source_SnakeGame_SnakeBoost_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_SnakeGame_SnakeBoost_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_SnakeGame_SnakeBoost_h_16_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_SnakeGame_SnakeBoost_h_16_SPARSE_DATA \
	Snake_Source_SnakeGame_SnakeBoost_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_SnakeGame_SnakeBoost_h_16_INCLASS_NO_PURE_DECLS \
	Snake_Source_SnakeGame_SnakeBoost_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASnakeBoost>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_SnakeGame_SnakeBoost_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
