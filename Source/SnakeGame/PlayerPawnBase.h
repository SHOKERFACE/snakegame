// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnake;
class AFood;
class ABonusScore;
class ASnakeBoost;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnake* SnakeActor;

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBoost* BoostActor;

	UPROPERTY(BlueprintReadWrite)
		ABonusScore* BonusScoreActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBoost> BoostActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonusScore> BonusScoreActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnake> SnakeActorClass;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	 
	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	void CreateSnakeActor();

	UFUNCTION()
		void HandPlayerVerticalInput(float value);
	UFUNCTION()
		void HandPlayerHorizontalInput(float value);

	UPROPERTY(EditAnyWhere)
	float MinX = -580.f;

	UPROPERTY(EditAnyWhere)
	float MaxX = 580.f;

	UPROPERTY(EditAnyWhere)
	float MinY = -580.f;

	UPROPERTY(EditAnyWhere)
	float MaxY = 580.f;

	UPROPERTY(EditAnyWhere)
	float SpawnZ = 0.f;

	UPROPERTY(EditAnyWhere)
	float SpawnTime = 2.5f;

	
	float BuferSpawnTime = 0.f;

	UPROPERTY(EditAnyWhere)
	float SpawnBonusTime=10.f;

	float BuferSpawnBonus=0.f;

	UPROPERTY(EditAnyWhere)
		float SpawnBoostTime = 10.f;

	float BuferBoostTime = 0.f;

	UFUNCTION()
	void RandomSpawnFood();

	UFUNCTION()
		void RandomSpawnBoost();


	UFUNCTION()
		void RandomSpawnBonus();


	UPROPERTY(EditAnyWhere)
	float Delay = 0.3f;

	float DeltaDelay = 0;

	int32 GameMode = 0;

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	int32 GetGameMode() const { return GameMode; }

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	int32 GetScore();

	UFUNCTION(BlueprintCallable, Category = "PlayerPawn")
	void DelayBoost();
};
