// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Snake.h"
#include "Components/InputComponent.h"
#include "Food.h"
#include "BonusScore.h"
#include "SnakeBoost.h"
// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	RandomSpawnFood();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BuferSpawnTime += DeltaTime;
	BuferSpawnBonus += DeltaTime;
	BuferBoostTime += DeltaTime;

	if (SpawnBonusTime < BuferSpawnBonus)
	{
		RandomSpawnBonus();
		BuferSpawnBonus = 0;
	}

	if (SpawnTime < BuferSpawnTime)
	{
		RandomSpawnFood();
		BuferSpawnTime = 0;
	}

	if (SpawnBoostTime < BuferBoostTime)
	{
		RandomSpawnBoost();
		BuferBoostTime = 0;
	}
	
	DeltaDelay += DeltaTime;

	
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandPlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandPlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnake>(SnakeActorClass, FTransform());
	GameMode = 1;
}

void APlayerPawnBase::HandPlayerVerticalInput(float value)
{
	
	if (IsValid(SnakeActor) && Delay < DeltaDelay )
	{
		if ( value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
			DeltaDelay = 0;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			DeltaDelay = 0;
		}
		
	}
}

void APlayerPawnBase::HandPlayerHorizontalInput(float value)
{
	
	if (IsValid(SnakeActor) && Delay < DeltaDelay)
	{
		if ( value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			DeltaDelay = 0;
		}
		else if ( value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			DeltaDelay = 0;
		}
		
	}
	
}

void APlayerPawnBase::RandomSpawnFood()
{
	FRotator FoodRotation = FRotator(0, 0, 0);
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);

	FVector SpawnFood = FVector(SpawnX, SpawnY, SpawnZ);
	if (IsValid(SnakeActor))
	{
		if (GetWorld())
		{
			
				AActor* NewActor = GetWorld()->SpawnActor(FoodActorClass, &SpawnFood, &FoodRotation);
		}	
	}
}

void APlayerPawnBase::RandomSpawnBoost()
{
	FRotator FoodRotation = FRotator(0, 0, 0);
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);

	FVector SpawnFood = FVector(SpawnX, SpawnY, SpawnZ);
	if (IsValid(SnakeActor))
	{
		if (GetWorld())
		{

			AActor* NewActor = GetWorld()->SpawnActor(BoostActorClass, &SpawnFood, &FoodRotation);

		}
	}
}

void APlayerPawnBase::RandomSpawnBonus()
{
	FRotator FoodRotation = FRotator(0, 0, 0);
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);

	FVector SpawnFood = FVector(SpawnX, SpawnY, SpawnZ);
	if (IsValid(SnakeActor))
	{
		if (GetWorld())
		{
				
				AActor* NewActor = GetWorld()->SpawnActor(BonusScoreActorClass, &SpawnFood, &FoodRotation);
	
		}
	}
}

int32 APlayerPawnBase::GetScore()
{
	if (IsValid(SnakeActor))
	{
		return SnakeActor->Score;
	}
	return int32();
}

void APlayerPawnBase::DelayBoost()
{
	if (Delay > 0.1)
	{
		Delay -= 0.02;
	}
}




