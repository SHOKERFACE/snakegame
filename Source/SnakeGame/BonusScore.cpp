// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusScore.h"
#include "Snake.h"
#include "PlayerPawnBase.h"
// Sets default values
ABonusScore::ABonusScore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusScore::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusScore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusScore::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			
			Destroy();
			Snake->Score++;
			Snake->Score++;
			Snake->Score++;
			Snake->Score++;
			Snake->Score++;
		}

	}
}

